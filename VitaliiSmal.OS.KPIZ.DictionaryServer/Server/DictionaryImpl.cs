﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dictionary;
using Grpc.Core;

namespace Server
{
    public sealed class DictionaryImpl : Dictionary.Dictionary.IDictionary
    {
        public Task<EngVal> Translate(UkrVal request, ServerCallContext context)
        {
            EngVal engVal = _dictionary.FindEngVal(request);
            return Task.FromResult(engVal);
        }

        public Task<Bool> Add(DictionaryNode request, ServerCallContext context)
        {
            _dictionary.AddNode(request);
            return Task.FromResult(new Bool { Value = true });
        }

        public Task<Bool> Remove(DictionaryNode request, ServerCallContext context)
        {
            _dictionary.RemoveNode(request);
            return Task.FromResult(new Bool { Value = true });
        }

        private readonly List<DictionaryNode> _dictionary = new List<DictionaryNode>();
    }
}
