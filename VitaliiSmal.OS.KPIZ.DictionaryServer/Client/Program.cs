﻿using System;

namespace Client
{
    public sealed class Program
    {
        private static void Main()
        {
            Console.Title = "CLIENT";

            var dictionary = new ClientDictionary();

            Console.WriteLine("Press to continue...");
            Console.ReadKey();
            Console.Clear();

            dictionary.Add("привіт", "hello");
            dictionary.Add("телефон", "phone");
            dictionary.Add("велосипед", "bike");

            Console.WriteLine(dictionary.Translate("привіт"));
            Console.WriteLine(dictionary.Translate("телефон"));
            Console.WriteLine(dictionary.Translate("велосипед"));

            dictionary.Remove("телефон", "phone");

            dictionary.Dispose();

            Console.ReadKey();
        }
    }
}
