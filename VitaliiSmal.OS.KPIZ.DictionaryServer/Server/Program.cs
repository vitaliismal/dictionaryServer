﻿using System;
using Grpc.Core;

namespace Server
{
    public sealed class Program
    {
        private const Int32 Port = 50051;

        private static void Main()
        {
            Console.Title = "SERVER";

            var server = new Grpc.Core.Server
            {
                Services = { Dictionary.Dictionary.BindService(new DictionaryImpl()) },
                Ports = { new ServerPort("localhost", Port, ServerCredentials.Insecure) }
            };

            server.Start();

            Console.WriteLine("Press any key to stop the server...");

            Console.ReadKey();

            server.ShutdownAsync().Wait();
        }
    }
}
