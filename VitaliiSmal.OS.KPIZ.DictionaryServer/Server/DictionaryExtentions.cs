﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dictionary;

namespace Server
{
    public static class DictionaryExtentions
    {
        public static EngVal FindEngVal(this List<DictionaryNode> dictionary, UkrVal ukr)
        {
            String ukrVal = ukr.Value;
            
            EngVal eng = dictionary.First(x => x.Ukr.Value == ukrVal).Eng;

            return eng;
        }

        public static void AddNode(this List<DictionaryNode> dictionary, DictionaryNode dictionaryNode)
        {
            if (!dictionary.Contains(dictionaryNode))
            {
                dictionary.Add(dictionaryNode);
            }

            String ukrVal = dictionaryNode.Ukr.Value;
            String engVal = dictionaryNode.Eng.Value;
            File.AppendAllText("../../../dictionary.txt", $"{ukrVal} - {engVal}" + Environment.NewLine);
        }

        public static void RemoveNode(this List<DictionaryNode> dictionary, DictionaryNode dictionaryNode)
        {
            dictionary.Remove(dictionaryNode);

            File.WriteAllText("../../../dictionary.txt", "");
            foreach (DictionaryNode node in dictionary)
            {
                dictionary.AddNode(node);
            }
        }

        private static void FillDictionary(this List<DictionaryNode> dictionary)
        {
            String[] lines = File.ReadAllLines("../../../dictionary.txt");
            foreach (String line in lines)
            {
                dictionary.AddNode(GenerateNode(line));
            }
        }

        private static DictionaryNode GenerateNode(String line)
        {
            String ukrVal = line.Split('-').First().Trim(' ');
            String engVal = line.Split('-').Last().Trim(' ');

            return new DictionaryNode
            {
                Ukr = new UkrVal
                {
                    Value = ukrVal
                },
                Eng = new EngVal
                {
                    Value = engVal
                }
            };
        }
    }
}
