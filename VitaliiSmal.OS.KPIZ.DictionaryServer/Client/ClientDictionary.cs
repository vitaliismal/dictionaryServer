﻿using System;
using Dictionary;
using Grpc.Core;

namespace Client
{
    public sealed class ClientDictionary : IDisposable
    {
        public ClientDictionary()
        {
            _channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);
            _client = Dictionary.Dictionary.NewClient(_channel);
        }

        public void Add(String ukr, String eng)
        {
            _client.Add(new DictionaryNode
            {
                Ukr = new UkrVal { Value = ukr },
                Eng = new EngVal { Value = eng }
            });
        }

        public String Translate(String ukr)
        {
            EngVal eng = _client.Translate(new UkrVal { Value = ukr });

            return eng.Value;
        }

        public void Remove(String ukr, String eng)
        {
            _client.Remove(new DictionaryNode
            {
                Ukr = new UkrVal { Value = ukr },
                Eng = new EngVal { Value = eng }
            });
        }

        public void Dispose()
        {
            _channel.ShutdownAsync().Wait();
        }

        private readonly Dictionary.Dictionary.DictionaryClient _client;
        private readonly Channel _channel;
    }
}
